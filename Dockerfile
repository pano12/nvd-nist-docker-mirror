FROM alpine/git
WORKDIR /app
RUN git clone https://github.com/stevespringett/nist-data-mirror.git

FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY --from=0 /app/nist-data-mirror /app
RUN mvn clean package

FROM httpd:alpine
WORKDIR /app
COPY --from=1 /app/target/nist-data-mirror.jar /app
RUN apk update && apk add --no-cache openjdk8-jre
RUN rm -f /usr/local/apache2/htdocs/*.html
RUN echo '0  1  *  *  *    java $JAVA_OPTS -jar /app/nist-data-mirror.jar /usr/local/apache2/htdocs json' > /etc/crontabs/root

# Running the recovery at launch, to not wait until 1am the next day to get data
CMD (java $JAVA_OPTS -jar /app/nist-data-mirror.jar /usr/local/apache2/htdocs) && ( crond -f -l 2 & ) && httpd -D FOREGROUND
